document.addEventListener('plusready', function() {
    var webview = plus.webview.currentWebview();
    var first = null;
    plus.key.addEventListener('backbutton', function() {
        var url = webview.getURL();
        var isEntry = false;
        if (url.indexOf('index.html') >= 0 || url.indexOf('login.html') >= 0) isEntry = true;
        webview.canBack(function(e) {
            if(e.canBack && !isEntry) {
                window.history.back(-1);
            } else {
                //处理逻辑：1秒内，连续两次按返回键，则退出应用；
                //首次按键，提示‘再按一次退出应用’
                if (!first) {
                    first = new Date().getTime();
                    plus.nativeUI.toast('再按一次退出应用');
                    setTimeout(function() {
                        first = null;
                    }, 1000);
                } else {
                    if (new Date().getTime() - first < 1500) {
                        plus.runtime.quit();
                    }
                }
            }
        })
    });
});